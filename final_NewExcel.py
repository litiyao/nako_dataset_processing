import os
import pydicom
import pandas as pd
import numpy as np




# Attributes is list of wanted parameters, for me Attributes = ['PatientID', 'StudyDate', 'StudyTime', 'StudyInstanceUID', 'InstitutionName', 'ProtocolName']
# path is where the new excel is saved (for me path = "/Users/tiyaoli/PycharmProjects/excel/NewExcel.xlsx")
# return dataframe parsing from all images and save it to new excel


def ParseAllImages(Attributes, AllImageData, path):

    LenAllImageData = len(AllImageData) #length of AllImageData
    DictofLists = {}
    for Attribute in Attributes:
        KeyName = Attribute
        DictofLists[KeyName] = []
        for DicomImage in AllImageData:
            ds = pydicom.dcmread(DicomImage) # read in dicom image
            a = ds.get(Attribute)
            DictofLists[KeyName].append(a)

    FinalArray = np.zeros((1, LenAllImageData)) #create a single row array with 0s
    for Attribute in Attributes:                       # stack the lists together, forming a 2D array
        CurrentArray = np.asarray(DictofLists[Attribute])
        FinalArray = np.vstack((FinalArray, CurrentArray))

    FinalArray = np.transpose(FinalArray)  # transpose
    FinalArray = FinalArray[:, 1:len(Attributes) + 1] # get rid of the first column full of 0s
    df = pd.DataFrame(FinalArray, columns=[Attribute for Attribute in Attributes]) # convert array into a dataframe
    df.to_excel(path, index=False) # write the dataframe to a new excel
    return df


# main function
#AllImageData = GetPathOfAllImages("/home/d1259/NAKO/")
#AllImageData = GetPathOfAllImages("/home/d1259/NAKO/") # directory where the images are saved

AllImageData = pd.read_excel('/home/d1259/no_backup/d1259/excel/AllImageData.xlsx', sheet_name='Sheet1')
AllImageData = AllImageData.iloc[:, 0].tolist()

Attributes = ['PatientID', 'StudyDate', 'StudyTime', 'StudyInstanceUID', 'StationName', 'ProtocolName']
path = '/home/d1259/no_backup/d1259/excel/NewExcel_for_AI/NewExcel.xlsx'
ParseAllImages(Attributes, AllImageData, path)
