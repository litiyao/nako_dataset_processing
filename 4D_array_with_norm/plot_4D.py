
import os
import time
import subprocess
import numpy as np
import pickle
import pandas
import pydicom
import numpy
import matplotlib

import matplotlib.pyplot as plt
from matplotlib import gridspec

def load_slices(list_in, list_opp, verbose = False):
    decomp = False
    dicom_dirs = [list_in, list_opp]
    channels = len(dicom_dirs)


    DirRef = list_in
    PathRef = list_in[0]
    RefDs = pydicom.dcmread(PathRef)  # 相当于pydicom.dcmread 读取第一个文件夹里第一个slice

    # Load dimensions:
    channels = 2
    ConstPixelDims = (int(RefDs.Rows),  # 行数 对我来说 260
                      int(RefDs.Columns),  # 列数 对我来说 320
                      len(DirRef),  # slice 数量 高度 316
                         channels)  # 频道数 水或脂肪 假设我是两个channel

    # Load spacing values (in mm):
    ConstPixelSpacing = (float(RefDs.PixelSpacing[0]),  # 1.40625
                         float(RefDs.PixelSpacing[1]),  # 1.40625
                         float(RefDs.SliceThickness))  # 3

    ArrayDicom = np.zeros(ConstPixelDims,
                        dtype='float16')  # 4维矩阵，每个点的数值都是0 Half precision float: sign bit, 5 bits exponent, 10 bits mantissa

    # walk through channels:
    for ch_index, ch_path in enumerate(dicom_dirs):
        lstPathsDCM = ch_path

        if verbose: print(" Loading " + str(ch_index) + "...")
        # loop through all the DICOM files
        for z, pathDCMch in enumerate(lstPathsDCM):
            # read the files
            ds = pydicom.dcmread(pathDCMch)
            # store the raw image data
            try:
                ArrayDicom[:, :, z, ch_index] = ds.pixel_array
            except NotImplementedError:
                decomp = True
                print("meet a problem")


        # normalize ArrayDicom:
    mx = [np.max(ArrayDicom[..., ch]) for ch in range(channels)]
    ArrayDicom = np.divide(ArrayDicom, mx)

        # print needed time in seconds



    return (ArrayDicom)




def plot_patient_slices(ch, dim, Array4D):
    """Function for plotting patient with label."""
    # get patient data
    #dicoms = np.load('/home/d1259/no_backup/d1259/excel/4D_array_with_norm/101935_30/101935_30.npy')  # dicoms是四维array niis 是标签
    dicoms = Array4D
    x, y, z = dicoms.shape[:3]  # x= 260, y = 320, z = 316
    # arrays for plotting
    dicoms = (dicoms).astype('float32')

    # channel: fat = 0, water = 1
    chNr = 0
    if ch == 'opp':
        chNr = 1

    # plot cuts in each dim
    fig = plt.figure(figsize=(18, 7))
    gs = gridspec.GridSpec(1, 3,
                           width_ratios=[x / y, z / y, x / z],
                           height_ratios=[1])
    ax1 = plt.subplot(gs[0])
    plt.axis('off')
    # plt.xlabel('x-axis')
    # plt.ylabel('y-axis')
    ax1.imshow(np.fliplr(np.rot90(dicoms[:, :, dim[2], chNr], axes=(1, 0))), interpolation='none', cmap='gray')
    ax2 = plt.subplot(gs[1])

    plt.axis('off')
    # plt.xlabel('z-axis')
    # plt.ylabel('y-axis')
    ax2.imshow(dicoms[dim[0], :, :, chNr], interpolation='none', cmap='gray')
    ax3 = plt.subplot(gs[2])
    plt.axis('off')
    # plt.xlabel('x-axis')
    # plt.ylabel('z-axis')
    ax3.imshow(np.fliplr(np.rot90(dicoms[:, dim[1], :, chNr], axes=(1, 0))), interpolation='none', cmap='gray')
    plt.show()

#Array4D = np.load('/home/d1259/no_backup/d1259/excel/4D_array_with_norm/101935_30/101935_30.npy')

list_in = pickle.load( open( "/home/d1259/no_backup/d1259/slices_path_in_opp/103889_30/103889_30_list_in.p", "rb" ) )
list_opp = pickle.load( open( "/home/d1259/no_backup/d1259/slices_path_in_opp/103889_30/103889_30_list_opp.p", "rb" ) )
Array4D = load_slices(list_in, list_opp, verbose = False)




ch = 'opp'
dim = (130, 160, 158, 2)
plot_patient_slices(ch, dim, Array4D)