import os
import time
import subprocess
import numpy as np
import pickle
import pandas
import pydicom
import numpy
import matplotlib

def load_slices(PatientID, verbose = False):

    list_in = pickle.load( open( "/home/d1259/no_backup/d1259/slices_path_in_opp/" + PatientID +"/" + PatientID + "_list_in.p", "rb" ) )
    list_opp = pickle.load(open("/home/d1259/no_backup/d1259/slices_path_in_opp/" + PatientID + "/"+ PatientID + "_list_opp.p", "rb"))
    decomp = False
    dicom_dirs = [list_in, list_opp]
    channels = len(dicom_dirs)


    DirRef = list_in
    PathRef = list_in[0]
    RefDs = pydicom.dcmread(PathRef)  # 相当于pydicom.dcmread 读取第一个文件夹里第一个slice

    # Load dimensions:
    channels = 2
    ConstPixelDims = (int(RefDs.Rows),  # 行数 对我来说 260
                      int(RefDs.Columns),  # 列数 对我来说 320
                      len(DirRef),  # slice 数量 高度 316
                         channels)  # 频道数 水或脂肪 假设我是两个channel

    # Load spacing values (in mm):
    ConstPixelSpacing = (float(RefDs.PixelSpacing[0]),  # 1.40625
                         float(RefDs.PixelSpacing[1]),  # 1.40625
                         float(RefDs.SliceThickness))  # 3

    ArrayDicom = np.zeros(ConstPixelDims,
                        dtype='float16')  # 4维矩阵，每个点的数值都是0 Half precision float: sign bit, 5 bits exponent, 10 bits mantissa

    # walk through channels:
    for ch_index, ch_path in enumerate(dicom_dirs):
        lstPathsDCM = ch_path

        if verbose: print(" Loading " + str(ch_index) + "...")
        # loop through all the DICOM files
        for z, pathDCMch in enumerate(lstPathsDCM):
            # read the files
            ds = pydicom.dcmread(pathDCMch)
            # store the raw image data
            try:
                ArrayDicom[:, :, z, ch_index] = ds.pixel_array
            except NotImplementedError:
                decomp = True
                print("meet a problem")


        # normalize ArrayDicom:
    #mx = [np.max(ArrayDicom[..., ch]) for ch in range(channels)]
    #ArrayDicom = np.divide(ArrayDicom, mx)

        # print needed time in seconds



    return (ArrayDicom)

#PatientFolders = pickle.load( open( "/home/d1259/no_backup/d1259/slices_path_in_opp/_list_opp_PatientFolders_path.p", "rb" ) )
list_patient_ID_new_train = pickle.load( open( "/home/d1259/no_backup/d1259/train_val_test/list_patient_ID_new_train.p", "rb" ) )
list_patient_ID_new_val = pickle.load( open( "/home/d1259/no_backup/d1259/train_val_test/list_patient_ID_new_val.p", "rb" ) )
list_patient_ID_new_test = pickle.load( open( "/home/d1259/no_backup/d1259/train_val_test/list_patient_ID_new_val.p", "rb" ) )

PatientFolders = list_patient_ID_new_train + list_patient_ID_new_val + list_patient_ID_new_test



PatientID_list = []
for PatientPath in PatientFolders[0:15]:
    PatientID = os.path.basename(PatientPath)
    PatientID_list.append(PatientID)
for PatientID in PatientID_list:
    Array4D = load_slices(PatientID, verbose = False)
    outfile = "/home/d1259/no_backup/d1259/2channel_no_norm/" + PatientID + ".npy"
    np.save(outfile, Array4D)