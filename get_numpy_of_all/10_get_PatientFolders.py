import os
import pandas as pd



# get the array of the paths of all patient folders
AllFilesAndFolders = os.listdir('/home/d1259/NAKO/')  # get all files and folders names in the specified directory
PatientFolders = []
namelength = 9
for PatientFolder in AllFilesAndFolders:
    if os.path.isdir(os.path.join(os.path.abspath("/home/d1259/NAKO/"), PatientFolder)):
        if len(PatientFolder) == namelength:
            PatientFolders.append(os.path.join(os.path.abspath("/home/d1259/NAKO/"), PatientFolder))

df = pd.DataFrame({'PatientFolders':PatientFolders})
df_sorted_path = df.sort_values(by=['PatientFolders'])

df_sub0 = df_sorted_path.iloc[0:40]
df_sub1 = df_sorted_path.iloc[40:80]
df_sub2 = df_sorted_path.iloc[80:120]
df_sub3 = df_sorted_path.iloc[120:160]
df_sub4 = df_sorted_path.iloc[160:200]

df_sorted_path.to_excel('/home/d1259/no_backup/d1259/excel/InPhase3DImages/PatientFolders.xlsx', index=False)
df_sub0.to_excel('/home/d1259/no_backup/d1259/excel/InPhase3DImages/PatientFolders0.xlsx', index=False)
df_sub1.to_excel('/home/d1259/no_backup/d1259/excel/InPhase3DImages/PatientFolders1.xlsx', index=False)
df_sub2.to_excel('/home/d1259/no_backup/d1259/excel/InPhase3DImages/PatientFolders2.xlsx', index=False)
df_sub3.to_excel('/home/d1259/no_backup/d1259/excel/InPhase3DImages/PatientFolders3.xlsx', index=False)
df_sub4.to_excel('/home/d1259/no_backup/d1259/excel/InPhase3DImages/PatientFolders4.xlsx', index=False)

NumberOfPatients = len(PatientFolders)
print(NumberOfPatients )
#print(PatientFolders)