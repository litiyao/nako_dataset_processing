import pydicom
import pandas as pd
import numpy as np
import os
def GetPathOfAllImages(directory): # directory where the images are saved, for me directory = '/home/d1259/NAKO/'
    PatientFolders = [directory]

    # get list of the paths of all sequence folders
    AllSequenceFolders = []
    for filename in PatientFolders:

        if os.listdir(filename):  #get rid of empty folder which stored under trash folder
            AFFsInPatientFolder = os.listdir(filename)  # list of AFFs (all files and folders) in one patient directory
            for AFF in AFFsInPatientFolder:
                if os.path.isdir(os.path.join(filename, AFF)):
                    AllSequenceFolders.append(os.path.join(filename, AFF))

    # get list of the paths of all images ALlImageData
    AllImageData = []
    for SequenceFolder in AllSequenceFolders:
        AFFsInSequenceFolder = os.listdir(SequenceFolder)  # list of AFFs (all files and folders) in one sequence directory
        for Image in AFFsInSequenceFolder:
            if os.path.isfile(os.path.join(SequenceFolder, Image)):
                AllImageData.append(os.path.join(SequenceFolder, Image))

    return AllImageData


def ParseImages(Attributes, AllImageData, path):
    LenAllImageData = len(AllImageData)  # length of AllImageData
    DictofLists = {}
    for Attribute in Attributes:
        KeyName = Attribute
        DictofLists[KeyName] = []
        for DicomImage in AllImageData:
            ds = pydicom.dcmread(DicomImage)  # read in dicom image
            a = ds.get(Attribute)
            DictofLists[KeyName].append(a)

    FinalArray = np.asarray(AllImageData)
    for Attribute in Attributes:  # stack the lists together, forming a 2D array
        CurrentArray = np.asarray(DictofLists[Attribute], dtype=object)
        FinalArray = np.vstack((FinalArray, CurrentArray))

    FinalArray = np.transpose(FinalArray)  # transpose
    Attributes.insert(0, 'SlicePath')
    df = pd.DataFrame(FinalArray, columns=[Attribute for Attribute in Attributes])  # convert array into a dataframe
    df.to_excel(path, index=False)  # write the dataframe to a new excel


#get slices paths of patient 100000_30
AllImageData = GetPathOfAllImages('/Users/tiyaoli/Desktop/NAKO/100000_30/')
# attributes varying in different slices
Attributes = ['ScanOptions', 'InstanceNumber', 'SeriesInstanceUID']
path = '/Users/tiyaoli/Desktop/ExcelTest/NewExcel_1_patient_SeriesInstanceUID.xlsx'
ParseImages(Attributes,AllImageData,path)









