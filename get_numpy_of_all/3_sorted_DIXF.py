import pandas

path = '/Users/tiyaoli/Desktop/ExcelTest/NewExcel_1_patient_scan_sequence.xlsx'

df = pandas.read_excel(path)
df_filtered = df[df['ScanOptions'] == 'DIXF']
df_filtered_sorted = df_filtered.sort_values(by=['InstanceNumber'])
df_sorted_path = df_filtered_sorted[['SlicePath']]


df_filtered_sorted.to_excel('/Users/tiyaoli/Desktop/ExcelTest/fat/sorted_DIXF_path.xlsx', index=False) # write the dataframe to a new excel


df_sorted_path.to_excel('/Users/tiyaoli/Desktop/ExcelTest/fat/sorted_DIXF_path_only.xlsx', index=False)

