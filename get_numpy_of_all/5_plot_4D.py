
import os
import time
import subprocess
import numpy as np
import pickle
import pandas
import pydicom
import numpy
import matplotlib

import matplotlib.pyplot as plt
from matplotlib import gridspec

Array4D = np.load('/Users/tiyaoli/Desktop/ExcelTest/Array4D.npy')


def plot_patient_slices(ch, dim):
    """Function for plotting patient with label."""
    # get patient data
    dicoms = np.load('/Users/tiyaoli/Desktop/ExcelTest/Array4D.npy')  # dicoms是四维array niis 是标签
    x, y, z = dicoms.shape[:3]  # x= 260, y = 320, z = 316
    # arrays for plotting
    dicoms = (dicoms).astype('float32')

    # channel: fat = 0, water = 1
    chNr = 0
    if ch == 'water':
        chNr = 1

    # plot cuts in each dim
    fig = plt.figure(figsize=(18, 7))
    gs = gridspec.GridSpec(1, 3,
                           width_ratios=[x / y, z / y, x / z],
                           height_ratios=[1])
    ax1 = plt.subplot(gs[0])
    plt.axis('off')
    # plt.xlabel('x-axis')
    # plt.ylabel('y-axis')
    ax1.imshow(np.fliplr(np.rot90(dicoms[:, :, dim[2], chNr], axes=(1, 0))), interpolation='none', cmap='gray')
    ax2 = plt.subplot(gs[1])

    plt.axis('off')
    # plt.xlabel('z-axis')
    # plt.ylabel('y-axis')
    ax2.imshow(dicoms[dim[0], :, :, chNr], interpolation='none', cmap='gray')
    ax3 = plt.subplot(gs[2])
    plt.axis('off')
    # plt.xlabel('x-axis')
    # plt.ylabel('z-axis')
    ax3.imshow(np.fliplr(np.rot90(dicoms[:, dim[1], :, chNr], axes=(1, 0))), interpolation='none', cmap='gray')
    plt.show()

ch = 'fat'
dim = (130, 160, 158, 2)
plot_patient_slices(ch, dim)