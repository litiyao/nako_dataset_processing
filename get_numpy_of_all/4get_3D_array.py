import pandas
import pydicom
import numpy
import pickle

path = '/Users/tiyaoli/Desktop/ExcelTest/sorted_DIXF_path.xlsx'
df = pandas.read_excel(path)
list_path = df['SlicePath'].tolist()

'''# 2D array plot
ds = pydicom.dcmread(list_path[0])
Array = ds.pixel_array
plt.imshow(Array, cmap = 'gray' )
plt.show()'''

listofarray = []
for filenameDCM in list_path:
    ds = pydicom.read_file(filenameDCM)
    Array = ds.pixel_array
    listofarray.append(Array)



'''ds0 = pydicom.dcmread(list_path[0])
Array0 = ds0.pixel_array

ds1 = pydicom.dcmread(list_path[1])
Array1 = ds1.pixel_array

#Array3D = numpy.vstack([ Array0, Array1 ])'''

# get 3D array
Array3D = numpy.array(listofarray)

# save 3D array to a pkl file
pickle.dump(Array3D, open("Array3D_DIXF.pkl", "wb"))

# convert it to stacked format using Pandas without header
stacked = pandas.Panel(Array3D.swapaxes(1,2)).to_frame().stack().reset_index()
stacked.columns = ['x', 'y', 'z', 'value']
# save to disk
stacked.to_csv('Array3D_without_header.csv', index=False, header=False)


#pickle.load(open("out.pkl", "rb"))
#numpy.savetxt('new.csv', Array3D, delimiter = ',')

