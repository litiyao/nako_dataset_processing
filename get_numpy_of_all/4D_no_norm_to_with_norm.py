import numpy as np

ArrayDicom = np.load('/home/d1259/no_backup/d1259/100001_30.npy')
channels = 2
mx = [np.max(ArrayDicom[..., ch]) for ch in range(channels)]
ArrayDicom = np.divide(ArrayDicom, mx)

np.save('/home/d1259/no_backup/d1259/100001_30.npy', ArrayDicom)


for i in range(96,100):
    patient = str(100000 + i) + '_30'
    ArrayDicom = np.load('/home/d1259/no_backup/d1259/2channel_array_with_norm/' + patient +'.npy')
    print(patient+'max='+str(ArrayDicom.max()))