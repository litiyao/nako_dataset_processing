
import os
import pydicom
import pandas as pd
import numpy as np

def GetPathOfAllImages(directory): # directory where the images are saved, for me directory = '/home/d1259/NAKO/'
    PatientFolders = [directory]

    # get list of the paths of all sequence folders
    AllSequenceFolders = []
    for filename in PatientFolders:

        if os.listdir(filename):  #get rid of empty folder which stored under trash folder
            AFFsInPatientFolder = os.listdir(filename)  # list of AFFs (all files and folders) in one patient directory
            for AFF in AFFsInPatientFolder:
                if os.path.isdir(os.path.join(filename, AFF)):
                    AllSequenceFolders.append(os.path.join(filename, AFF))

    # get list of the paths of all images ALlImageData
    AllImageData = []
    for SequenceFolder in AllSequenceFolders:
        AFFsInSequenceFolder = os.listdir(SequenceFolder)  # list of AFFs (all files and folders) in one sequence directory
        for Image in AFFsInSequenceFolder:
            if os.path.isfile(os.path.join(SequenceFolder, Image)):
                AllImageData.append(os.path.join(SequenceFolder, Image))

    return AllImageData


def ParseImages(Attributes, AllImageData, path):
    LenAllImageData = len(AllImageData)  # length of AllImageData
    DictofLists = {}
    for Attribute in Attributes:
        KeyName = Attribute
        DictofLists[KeyName] = []
        for DicomImage in AllImageData:
            ds = pydicom.dcmread(DicomImage)  # read in dicom image
            a = ds.get(Attribute)
            DictofLists[KeyName].append(a)

    FinalArray = np.asarray(AllImageData)
    for Attribute in Attributes:  # stack the lists together, forming a 2D array
        CurrentArray = np.asarray(DictofLists[Attribute], dtype=object)
        FinalArray = np.vstack((FinalArray, CurrentArray))

    FinalArray = np.transpose(FinalArray)  # transpose
    Attributes.insert(0, 'SlicePath')
    df = pd.DataFrame(FinalArray, columns=[Attribute for Attribute in Attributes])  # convert array into a dataframe
    df.to_excel(path, index=False)  # write the dataframe to a new excel


#get slices paths of patient 100000_30
AllImageData = GetPathOfAllImages('/Users/tiyaoli/Desktop/NAKO/100000_30/')
# attributes varying in different slices
Attributes = ['SOPInstanceUID', 'ContentTime', 'ScanOptions', 'InstanceNumber', 'SliceLocation', 'LargestImagePixelValue', 'WindowCenter', 'WindowWidth']
path = '/Users/tiyaoli/Desktop/ExcelTest/patient_100000_30_Path_all_varying_attributes.xlsx'
ParseImages(Attributes,AllImageData,path)

# excel file containing only ScanOptions and InstanceNumber
Attributes = ['ScanOptions','InstanceNumber']
path = '/Users/tiyaoli/Desktop/ExcelTest/patient_100000_30_Path_ScanOptions_InstanceNUmber.xlsx'
ParseImages(Attributes,AllImageData,path)

# sort the slices ScanOptions
path = '/Users/tiyaoli/Desktop/ExcelTest/patient_100000_30_Path_ScanOptions_InstanceNUmber.xlsx'
df = pd.read_excel(path)
df_sorted_by_ScanOptions = df.sort_values(by=['ScanOptions'])
path = '/Users/tiyaoli/Desktop/ExcelTest/sorted_by_ScanOptions.xlsx'
df_sorted_by_ScanOptions.to_excel(path, index=False) # write the dataframe to a new excel

# choose fat image and sort the slices by InstanceNumber
df_filtered = df[df['ScanOptions'] == 'DIXF']
df_filtered_sorted = df_filtered.sort_values(by=['InstanceNumber'])
path = '/Users/tiyaoli/Desktop/ExcelTest/filtered_by_DIXF_sorted_by_InstanceNumber.xlsx'
df_filtered_sorted.to_excel(path, index=False)

# path of DIXF slices sorted by InstanceNumber
df = pd.read_excel(path)
df_sorted_path = df[['SlicePath']]
path = '/Users/tiyaoli/Desktop/ExcelTest/sorted_DIXF_path.xlsx'
df.to_excel(path, index=False)









