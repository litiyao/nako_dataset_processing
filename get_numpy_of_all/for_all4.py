
import os
import pydicom
import pandas as pd
import numpy as np
import time
import subprocess
import pandas
import numpy

# measure time
t0 = time.time()

def GetPathOfAllImages(directory): # directory where the images are saved, for me directory = '/home/d1259/NAKO/'
    PatientFolders = [directory]

    # get list of the paths of all sequence folders
    AllSequenceFolders = []
    for filename in PatientFolders:

        if os.listdir(filename):  #get rid of empty folder which stored under trash folder
            AFFsInPatientFolder = os.listdir(filename)  # list of AFFs (all files and folders) in one patient directory
            for AFF in AFFsInPatientFolder:
                if os.path.isdir(os.path.join(filename, AFF)):
                    AllSequenceFolders.append(os.path.join(filename, AFF))

    # get list of the paths of all images ALlImageData
    AllImageData = []
    for SequenceFolder in AllSequenceFolders:
        AFFsInSequenceFolder = os.listdir(SequenceFolder)  # list of AFFs (all files and folders) in one sequence directory
        for Image in AFFsInSequenceFolder:
            if os.path.isfile(os.path.join(SequenceFolder, Image)):
                AllImageData.append(os.path.join(SequenceFolder, Image))

    return AllImageData


def ParseImages(Attributes, AllImageData, path):
    LenAllImageData = len(AllImageData)  # length of AllImageData
    DictofLists = {}
    for Attribute in Attributes:
        KeyName = Attribute
        DictofLists[KeyName] = []
        for DicomImage in AllImageData:
            ds = pydicom.dcmread(DicomImage)  # read in dicom image
            a = ds.get(Attribute)
            DictofLists[KeyName].append(a)

    FinalArray = np.asarray(AllImageData)
    for Attribute in Attributes:  # stack the lists together, forming a 2D array
        CurrentArray = np.asarray(DictofLists[Attribute], dtype=object)
        FinalArray = np.vstack((FinalArray, CurrentArray))

    FinalArray = np.transpose(FinalArray)  # transpose
    Attributes.insert(0, 'SlicePath')
    df = pd.DataFrame(FinalArray, columns=[Attribute for Attribute in Attributes])  # convert array into a dataframe
    df.to_excel(path, index=False)  # write the dataframe to a new excel

def get_PatientFolders(directory):
    AFFsInNAKO = os.listdir(directory)  # get all files and folders names in the specified directory NAKO
    PatientFolders = []
    for PatientFolder in AFFsInNAKO:
        if len(PatientFolder) == 9:
            if os.path.isdir(os.path.join(os.path.abspath(directory), PatientFolder)): # check whether the current object is a folder or not
                PatientFolders.append(os.path.join(os.path.abspath(directory), PatientFolder))
    return PatientFolders

def load_slices(verbose=False):

    decomp = False
    dicom_dirs = [list_in, list_opp]
    channels = len(dicom_dirs)


    DirRef = list_in
    PathRef = list_in[0]
    RefDs = pydicom.dcmread(PathRef)  # 相当于pydicom.dcmread 读取第一个文件夹里第一个slice

    # Load dimensions:
    channels = 2
    ConstPixelDims = (int(RefDs.Rows),  # 行数 对我来说 260
                      int(RefDs.Columns),  # 列数 对我来说 320
                      len(DirRef),  # slice 数量 高度 316
                         channels)  # 频道数 水或脂肪 假设我是两个channel

    # Load spacing values (in mm):
    ConstPixelSpacing = (float(RefDs.PixelSpacing[0]),  # 1.40625
                         float(RefDs.PixelSpacing[1]),  # 1.40625
                         float(RefDs.SliceThickness))  # 3

    ArrayDicom = np.zeros(ConstPixelDims,
                        dtype='float16')  # 4维矩阵，每个点的数值都是0 Half precision float: sign bit, 5 bits exponent, 10 bits mantissa

    # walk through channels:
    for ch_index, ch_path in enumerate(dicom_dirs):
        lstPathsDCM = ch_path

        if verbose: print(" Loading " + str(ch_index) + "...")
        # loop through all the DICOM files
        for z, pathDCMch in enumerate(lstPathsDCM):
            # read the files
            ds = pydicom.dcmread(pathDCMch)
            # store the raw image data
            try:
                ArrayDicom[:, :, z, ch_index] = ds.pixel_array
            except NotImplementedError:
                decomp = True
                print("meet a problem")


        # normalize ArrayDicom:
    mx = [np.max(ArrayDicom[..., ch]) for ch in range(channels)]
    ArrayDicom = np.divide(ArrayDicom, mx)

        # print needed time in seconds
    nd_time = time.time() - t0
    if verbose:
        if decomp:
            print("Needed time: " + str(nd_time) + "s." + " Files had to be decompressed.")
        else:
            print("Needed time: " + str(nd_time) + "s.")

    return (ArrayDicom)

df = pandas.read_excel('/home/d1259/no_backup/d1259/excel/InPhase3DImages/PatientFolders4.xlsx')
PatientFolders = df['PatientFolders'].tolist()


for PatientPath in PatientFolders:

    #get slices paths of patient 100000_30
    AllImageData = GetPathOfAllImages(PatientPath)
    patient = os.path.basename(PatientPath)


    # excel file containing only ScanOptions and InstanceNumber
    Attributes = ['ScanOptions','InstanceNumber']
    path = '/home/d1259/no_backup/d1259/excel/InPhase3DImages/' + patient +'_Path_ScanOptions_InstanceNUmber.xlsx'
    ParseImages(Attributes,AllImageData,path)

    # sort the slices ScanOptions

    path = '/home/d1259/no_backup/d1259/excel/InPhase3DImages/' + patient +'_Path_ScanOptions_InstanceNUmber.xlsx'
    df = pd.read_excel(path)
    df_sorted_by_ScanOptions = df.sort_values(by=['ScanOptions'])
    path = '/home/d1259/no_backup/d1259/excel/InPhase3DImages/' + patient + 'sorted_by_ScanOptions.xlsx'
    df_sorted_by_ScanOptions.to_excel(path, index=False) # write the dataframe to a new excel

    # choose fat image and sort the slices by InstanceNumber
    df_filtered = df[~df['ScanOptions'].isin(['DIXF','DIXW'])]
    df_filtered_sorted = df_filtered.sort_values(by=['InstanceNumber'])
    df_sorted_path = df_filtered_sorted[['SlicePath']]

    df = df_sorted_path
    list_path = df['SlicePath'].tolist()
    list_in = list_path[0:79] + list_path[79*2:79*3] + list_path[79*4:79*5] + list_path[79*6:79*7] # in phase
    list_opp = list_path[79:79*2] + list_path[79*3:79*4] + list_path[79*5:79*6] + list_path[79*7:79*8] # opp phase


    p = load_slices(verbose=False)

    numpy.save('/home/d1259/no_backup/d1259/excel/InPhase3DImages/' + patient +'.npy', p)










