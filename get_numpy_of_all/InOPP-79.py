
# coding: utf-8

# In[7]:


import pandas

path = '/Users/tiyaoli/Desktop/ExcelTest/NewExcel_1_patient_scan_sequence.xlsx'

df = pandas.read_excel(path)
df_filtered = df[~df['ScanOptions'].isin(['DIXF','DIXW'])]
df_filtered_sorted = df_filtered.sort_values(by=['InstanceNumber'])
df_sorted_path = df_filtered_sorted[['SlicePath']]


# In[8]:


df_filtered_sorted.to_excel('/Users/tiyaoli/Desktop/ExcelTest/InOPP/sorted_InOpp_path.xlsx', index=False) # write the dataframe to a new excel

df_sorted_path.to_excel('/Users/tiyaoli/Desktop/ExcelTest/InOpp/sorted_InOpp_path_only.xlsx', index=False)


# In[9]:


df = pandas.read_excel('/Users/tiyaoli/Desktop/ExcelTest/InOPP/sorted_InOpp_path.xlsx')
list_path = df['SlicePath'].tolist()


# In[14]:


list_in = list_path[0:79] + list_path[79*2:79*3] + list_path[79*4:79*5] + list_path[79*6:79*7] # in phase
list_opp = list_path[79:79*2] + list_path[79*3:79*4] + list_path[79*5:79*6] + list_path[79*7:79*8] # opp phase


# In[15]:


len(list_in)


# In[16]:


len(list_opp)


# In[17]:


df_in = pandas.DataFrame({'SlicePath':list_in})
df_in.to_excel('/Users/tiyaoli/Desktop/ExcelTest/InOPP/sorted_In_path_only.xlsx', index=False)

df_opp = pandas.DataFrame({'SlicePath':list_opp})
df_opp.to_excel('/Users/tiyaoli/Desktop/ExcelTest/InOPP/sorted_Opp_path_only.xlsx', index=False)

