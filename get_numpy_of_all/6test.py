import numpy as np
import pandas as pd
import pickle

Array3D = pickle.load(open("out.pkl", "rb"))
# create an example array
a = np.arange(24).reshape([2,3,4])
# convert it to stacked format using Pandas
stacked = pd.Panel(a.swapaxes(1,2)).to_frame().stack().reset_index()
stacked.columns = ['x', 'y', 'z', 'value']
# save to disk
stacked.to_csv('stacked.csv', index=False)