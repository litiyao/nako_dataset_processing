import os
import time
import subprocess
import numpy as np
import pickle
import pandas
import pydicom
import numpy
import matplotlib
matplotlib.use('Agg') # Must be before importing matplotlib.pyplot or pylab!
import matplotlib.pyplot as plt
from matplotlib import gridspec

def load_slices(verbose=False):
    """Load patients data (dicom- and nii-files) in numpy arrays."""
    # measure time
    t0 = time.time()
    decomp = False
    dicom_dirs = ['/Users/tiyaoli/Desktop/ExcelTest/fat/sorted_DIXF_path_only.xlsx','/Users/tiyaoli/Desktop/ExcelTest/water/sorted_DIXW_path_only.xlsx']
    channels = len(dicom_dirs)

    df = pandas.read_excel(dicom_dirs[0])
    list_path = df['SlicePath'].tolist()
    # get reference for patient:
    DirRef = list_path
    PathRef = list_path[0]
    RefDs = pydicom.dcmread(PathRef)  # 相当于pydicom.dcmread 读取第一个文件夹里第一个slice

    # Load dimensions:
    ConstPixelDims = (int(RefDs.Rows),  # 行数 对我来说 260
                      int(RefDs.Columns),  # 列数 对我来说 320
                      len(DirRef),  # slice 数量 高度 316
                       channels)  # 频道数 水或脂肪 假设我是两个channel

    # Load spacing values (in mm):
    ConstPixelSpacing = (float(RefDs.PixelSpacing[0]),  # 1.40625
                         float(RefDs.PixelSpacing[1]),  # 1.40625
                         float(RefDs.SliceThickness))  # 3

    ArrayDicom = np.zeros(ConstPixelDims,
                          dtype='float16')  # 4维矩阵，每个点的数值都是0 Half precision float: sign bit, 5 bits exponent, 10 bits mantissa

    # walk through channels:
    for ch_index, ch_path in enumerate(dicom_dirs):
        df = pandas.read_excel(dicom_dirs[ch_index])
        lstPathsDCM = df['SlicePath'].tolist()

        if verbose: print(" Loading " + ch_path + "...")
        # loop through all the DICOM files
        for z, pathDCMch in enumerate(lstPathsDCM):
            # read the files
            ds = pydicom.dcmread(pathDCMch)
            # store the raw image data
            try:
                ArrayDicom[:, :, z, ch_index] = ds.pixel_array
            except NotImplementedError:
                decomp = True
                print("meet a problem")


    # normalize ArrayDicom:
    mx = [np.max(ArrayDicom[..., ch]) for ch in range(channels)]
    ArrayDicom = np.divide(ArrayDicom, mx)

    # print needed time in seconds
    nd_time = time.time() - t0
    if verbose:
        if decomp:
            print("Needed time: " + str(nd_time) + "s." + " Files had to be decompressed.")
        else:
            print("Needed time: " + str(nd_time) + "s.")

    return (ArrayDicom)

p = load_slices(verbose= True)
numpy.save('/Users/tiyaoli/Desktop/ExcelTest/Array4D.npy', p)
q = np.load('/Users/tiyaoli/Desktop/ExcelTest/Array4D.npy')