load('/Users/tiyaoli/PycharmProjects/excel/get_labels_for_all_slices/labels.mat');
% change to double
all_labels_d = double(all_labels);
unique_labels_d = double(unique_labels);
% transpose
all_labels_d_T = transpose(all_labels_d);
unique_labels_d_T = transpose(unique_labels_d);
% save 2 arrays to a cell
cLabels = {all_labels_d_T,unique_labels_d_T};
% save cell
save('/Users/tiyaoli/PycharmProjects/excel/get_labels_for_all_slices/cLabels.mat', 'cLabels');



