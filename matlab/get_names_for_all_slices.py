import os
import pandas as pd
import numpy as np
import scipy.io as sio
import pickle

# get the array of the paths of all patient folders
Unique_Name_List = pickle.load( open( "patients.pickle", "rb" ) )
Unique_Name_List = os.listdir('D:/in_phase_with_norm_new_dataset_mat')
Unique_Name_Array = np.asarray(Unique_Name_List)



all_names = np.repeat(Unique_Name_List, 316)# get all files and folders names in the specified directory
data={'all_names':all_names, 'Unique_Name_Array':Unique_Name_Array}
sio.savemat('names.mat',data)


