load('names.mat');
load('labels.mat');

% LabeledSamplesInformationUnique
Name = cellstr(Unique_Name_Array);
Name = reshape(Name,[120 1]);
Name = reshape(Name,[1 120]);

unique_labels = double(unique_labels);
unique_labels = num2cell(unique_labels);
Label = reshape(unique_labels,[120 1]);
Label = reshape(Label,[1 120]);

field1 = 'Name'; value1 = Name ;
field2 = 'Label'; value2 = Label;
LabeledSamplesInformationUnique = struct(field1,value1,field2,value2);


% LabeledSamplesInformation
Name = cellstr(all_names);
Name = reshape(Name,[37920 1]);
Name = reshape(Name,[1 37920]);

all_labels = double(all_labels);
all_labels = num2cell(all_labels);
Label = reshape(all_labels,[37920 1]);
Label = reshape(all_labels,[1 37920]);

field1 = 'Name'; value1 = Name ;
field2 = 'Label'; value2 = Label;
LabeledSamplesInformation = struct(field1,value1,field2,value2);

save('LabeledSamplesInformation.mat','LabeledSamplesInformation','LabeledSamplesInformationUnique');
