
import os
import pydicom
import pandas as pd
import numpy as np

def GetPathOfFirstSlice(directory): # directory where the images are saved, for me directory = '/home/d1259/NAKO/'
    # get the array of the paths of all patient folders
    AFFsInNAKO = os.listdir(directory)  # get all files and folders names in the specified directory NAKO
    PatientFolders = []
    for PatientFolder in AFFsInNAKO:
        if os.path.isdir(os.path.join(os.path.abspath(directory), PatientFolder)): # check whether the current object is a folder or not
            PatientFolders.append(os.path.join(os.path.abspath(directory), PatientFolder))

    AllSequenceFolders = []
    for filename in PatientFolders:

        if os.listdir(filename):  # get rid of empty folder which stored under trash folder
            AFFsInPatientFolder = os.listdir(filename)  # list of AFFs (all files and folders) in one patient directory
            for AFF in AFFsInPatientFolder:
                if os.path.isdir(os.path.join(filename, AFF)):
                    AllSequenceFolders.append(os.path.join(filename, AFF))

    # get list of the paths of all fisrt slices
    AllFirstSlices = []
    for firstfolder in  AllSequenceFolders:
        firstslice = next(os.path.join(firstfolder, f) for f in os.listdir(firstfolder) if os.path.isfile(os.path.join(firstfolder, f)))
        AllFirstSlices.append(os.path.join(firstfolder, firstslice))

    return AllFirstSlices

# Attributes is list of wanted parameters, for me Attributes = ['PatientID', 'StudyDate', 'StudyTime', 'StudyInstanceUID', 'InstitutionName', 'ProtocolName']
# path is where the new excel is saved (for me path = "/Users/tiyaoli/PycharmProjects/excel/NewExcel.xlsx")
# return dataframe parsing from all images and save it to new excel


def ParseImages(Attributes, AllFistSlices, path):

    LenAllImageData = len(AllFistSlices)  # length of AllImageData
    DictofLists = {}
    for Attribute in Attributes:
        KeyName = Attribute
        DictofLists[KeyName] = []
        for DicomImage in AllFistSlices:
            ds = pydicom.dcmread(DicomImage) # read in dicom image
            a = ds.get(Attribute)
            DictofLists[KeyName].append(a)

    FinalArray = np.zeros((1, LenAllImageData))  # create a single row array with 0s
    for Attribute in Attributes:                       # stack the lists together, forming a 2D array
        CurrentArray = np.asarray(DictofLists[Attribute])
        FinalArray = np.vstack((FinalArray, CurrentArray))

    FinalArray = np.transpose(FinalArray)  # transpose
    FinalArray = FinalArray[:, 1:len(Attributes) + 1] # get rid of the first column full of 0s
    df = pd.DataFrame(FinalArray, columns=[Attribute for Attribute in Attributes]) # convert array into a dataframe
    df.to_excel(path, index=False) # write the dataframe to a new excel
    return df

# PathOfOldExcel (for me '/Users/tiyaoli/PycharmProjects/excel/visualQADump-MEVISDB_2014-2015-2016.v2.xlsx')
# SheetName (for me 'visQA_GRE3D' )
# combine PatientID_ExcelFile and QualityRating from old excel in new excel, get a CombiExcel
# return the final dataframe
# PathOfCombiExcel (for me "/Users/tiyaoli/PycharmProjects/excel/CombiExel.xlsx")
def CombiExcel(PathOfOldExcel, SheetName, PathOfNewExcel, PathOfCombiExcel):
    # read in old excel
    OldExcel = pd.read_excel(PathOfOldExcel, sheet_name=SheetName)
    PartOfOldExcel = OldExcel['studyInstanceUID']  # choose studyInstanceUID from old excel
    PartOfOldExcel.columns = \
        ['StudyInstanceUID']  # rename the label to keep consistent with label in new excel obtained by parsing images

    # read in new excel
    NewExcel = pd.read_excel(PathOfNewExcel)
    PartOfNewExcel = NewExcel['StudyInstanceUID']

    # get the list of PatientID_ExcelFile from old excel matched into new excel
    PatientID_ExcelFile = []
    NumberOfRowsNE = len(NewExcel.index)  # count rows in new excel
    NumberOfRowsOE = len(OldExcel.index)  # count rows in old excel
    for RowInNE in range(NumberOfRowsNE):  # RowInNE row in new excel
        for RowInOE in range(NumberOfRowsOE):
            row1 = PartOfNewExcel.iloc[RowInNE]
            row2 = PartOfOldExcel.iloc[RowInOE]
            if row1 == row2:
                PatientIDInNE = OldExcel.at[RowInOE, 'patid']
                PatientID_ExcelFile.append(PatientIDInNE)

    # get the list of QualityRating from old excel matched into new excel
    QualityRating = []
    NumberOfRowsNE = len(NewExcel.index)  # count rows in new excel
    NumberOfRowsOE = len(OldExcel.index)  # count rows in old excel
    for RowInNE in range(NumberOfRowsNE):  # RowInNE row in new excel
        for RowInOE in range(NumberOfRowsOE):
            row1 = PartOfNewExcel.iloc[RowInNE]
            row2 = PartOfOldExcel.iloc[RowInOE]
            if row1 == row2:
                QRInNE = OldExcel.at[RowInOE, 'qualityRating']
                QualityRating.append(QRInNE)

    NewExcel.insert(0, 'PatientID_ExcelFile', PatientID_ExcelFile)  # add PatientID_ExcelFile as the first column
    NewExcel['QualityRating'] = QualityRating  # add QualityRating as the last column
    NewExcel = NewExcel.rename(columns={'PatientID': 'PatientID_FolderName', 'ProtocolName': 'Protocol',
                                        'StationName': 'InstitutionName'})  # rename

    # write the dataframe to excel with the name CombiExcel
    NewExcel.to_excel(PathOfCombiExcel, index=False)
    return NewExcel


# main function
# just need to modify the parameters below
AllFirstSlices = GetPathOfFirstSlice("/home/d1259/NAKO/")  # directory where the images are saved

Attributes = ['PatientID', 'StudyDate', 'StudyTime', 'StudyInstanceUID', 'StationName',
              'ProtocolName']  # attributes that wir are intrested in in images
path = '/home/d1259/no_backup/d1259/excel/final3_results/NewExcel.xlsx'  # path to where the new excel should be saved
Parse = ParseImages(Attributes, AllFirstSlices, path)

PathOfOldExcel = '/home/d1259/no_backup/d1259/ExcelTest/visualQADump-MEVISDB_2014-2015-2016.v2.xlsx'
SheetName = 'visQA_GRE3D'
PathOfNewExcel = '/home/d1259/no_backup/d1259/excel/final3_results/NewExcel.xlsx'  # save path of the new excel
PathOfCombiExcel = '/home/d1259/no_backup/d1259/excel/final3_results/CombiExcel.xlsx'  # save path of the combined excel
CombiExcel(PathOfOldExcel, SheetName, PathOfNewExcel, PathOfCombiExcel)


