import os
import pydicom

# get the array of the paths of all patient folders
AFFsInNAKO = os.listdir('/home/d1259/NAKO/')  # get all files and folders names in the specified directory NAKO
PatientFolders = []
for PatientFolder in AFFsInNAKO:
    if os.path.isdir(os.path.join(os.path.abspath("/home/d1259/NAKO/"), PatientFolder)): # check whether the current object is a folder or not
    PatientFolders.append(os.path.join(os.path.abspath("/home/d1259/NAKO/"), PatientFolder)) #include 200 patient folders and 1 trash folder ('/home/d1259/NAKO/.Trash-1000')
#NumberOfPatients = len(PatientFolders)
#print(NumberOfPatients)
#print(PatientFolders)



# get list of the paths of all sequence folders
AllSequenceFolders = []
for filename in PatientFolders:

    if os.listdir(filename):  #get rid of empty folder which stored under trash folder
        AFFsInPatientFolder = os.listdir(filename)  # list of AFFs (all files and folders) in one patient directory
        for AFF in AFFsInPatientFolder:
            if os.path.isdir(os.path.join(filename, AFF)):
                AllSequenceFolders.append(os.path.join(filename, AFF))

#print(len(AllSequenceFolders))     #200
#print(AllSequenceFolders)


# get array of the paths of all images ALlImageData
AllImageData = []
for SequenceFolder in AllSequenceFolders:
    AFFsInSequenceFolder = os.listdir(SequenceFolder)  # list of AFFs (all files and folders) in one sequence directory
    for Image in AFFsInSequenceFolder:
        if os.path.isfile(os.path.join(SequenceFolder, Image)):
            AllImageData.append(os.path.join(SequenceFolder, Image))
#print(len(AllImageData)) #252801 images
#print(AllImageData)

ds = pydicom.dcmread(AllImageData[0])
