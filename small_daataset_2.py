import os
import pydicom
import pandas
import xlsxwriter
AllSequenceFolders = ['/home/d1259/no_backup/d1259/NAKO_Rest/3D_GRE_TRA_W_COMPOSED_-17/', '/home/d1259/no_backup/d1259/NAKO_Rest/3D_GRE_TRA_W_COMPOSED_-41/']
# get array of the paths of all images ALlImageData
AllImageData = []

for SequenceFolder in AllSequenceFolders:
    AFFsInSequenceFolder = os.listdir(SequenceFolder)  # list of AFFs (all files and folders) in one sequence directory
    for Image in AFFsInSequenceFolder:
        if os.path.isfile(os.path.join(SequenceFolder, Image)):
            AllImageData.append(os.path.join(SequenceFolder, Image))
print(len(AllImageData)) #2529 images
print(AllImageData)

#ds = pydicom.dcmread(AllImageData[0])

#from excel
#df = pandas.ExcelFile('/home/d1259/no_backup/d1259/excel/visualQADump-MEVISDB_2014-2015-2016.v2.xlsx').parse('visQA_GRE3D')

for dcm_file in AllImageData:
    ds = pydicom.dcmread(dcm_file)
    workbook = xlsxwriter.Workbook('/home/d1259/no_backup/d1259/excel/' + 'Li3.xlsx')
    worksheet = workbook.add_worksheet()

    data = (
            ["PatientID", ds.get("PatientID", "None")],
            ["StudyDate", ds.get("StudyDate", "None")],
            ["StudyTime", ds.get("StudyTime", "None")]
           )

    row = 0
    col = 0

    for name, value in (data):
        worksheet.write(5, 6,     name)
        worksheet.write(5, 7, value)
    row += 1

workbook.close()