import os
import pydicom
import pandas as pd
import numpy as np

def GetPathOfSubfolders(directory): # directory where the images are saved, for me directory = '/home/d1259/NAKO/'
    # get the array of the paths of all patient folders
    AFFsInNAKO = os.listdir(directory)  # get all files and folders names in the specified directory NAKO
    PatientFolders = []
    for PatientFolder in AFFsInNAKO:
        if os.path.isdir(os.path.join(os.path.abspath(directory), PatientFolder)): # check whether the current object is a folder or not
            PatientFolders.append(os.path.join(os.path.abspath(directory), PatientFolder))

    AllSequenceFolders = []
    for filename in PatientFolders:

        if os.listdir(filename):  # get rid of empty folder which stored under trash folder
            AFFsInPatientFolder = os.listdir(filename)  # list of AFFs (all files and folders) in one patient directory
            for AFF in AFFsInPatientFolder:
                if os.path.isdir(os.path.join(filename, AFF)):
                    AllSequenceFolders.append(os.path.join(filename, AFF))
    return AllSequenceFolders
AllSequenceFolders = GetPathOfSubfolders("/home/d1259/NAKO/")
print(AllSequenceFolders)

list_length = []
for SequenceFolder in AllSequenceFolders:
    AllImageData = []
    AFFsInSequenceFolder = os.listdir(SequenceFolder)  # list of AFFs (all files and folders) in one sequence directory
    for Image in AFFsInSequenceFolder:
        if os.path.isfile(os.path.join(SequenceFolder, Image)):
            AllImageData.append(os.path.join(SequenceFolder, Image))

    print(len(AllImageData))
    list_length.append(len(AllImageData))
df = pd.DataFrame({'SliceNumber':list_length})
df.to_excel('/home/d1259/no_backup/d1259/excel/final3_results/SliceNumber.xlsx', index=False)
