
import os
import pydicom
import numpy as np

filename = '/home/d1259/med_data/NAKO/NAKO_IQA/Q12/dicom_sorted/'
AllSequenceFolders = []
AFFsInPatientFolder = os.listdir(filename)  # list of AFFs (all files and folders) in one patient directory
for AFF in AFFsInPatientFolder:
    if os.path.isdir(os.path.join(filename, AFF)):
        AllSequenceFolders.append(os.path.join(filename, AFF))

COMPOSED_folders = []
for folder in AllSequenceFolders:
    if 'COMPOSED' in folder:
        COMPOSED_folders.append(folder)

def get_Patient(directory):
    AFFsInNAKO = os.listdir(directory)  # get all files and folders names in the specified directory NAKO
    AFFsInNAKO.sort()
    Patient_slices = []
    for slice in AFFsInNAKO:

        Patient_slices.append(os.path.join(os.path.abspath(directory), slice))
    return Patient_slices

def get_array(COMPOSED_folder,filename):
    patient_slices = get_Patient(COMPOSED_folder)

    dicom_dirs = patient_slices

    RefDs = pydicom.dcmread(patient_slices[0])
    p = int(RefDs.Rows)
    channels = 1
    ConstPixelDims = (int(RefDs.Rows),  # 行数 对我来说 260
                          int(RefDs.Columns),  # 列数 对我来说 320
                          len(dicom_dirs),  # slice 数量 高度 316
                             channels)

    ArrayDicom = np.zeros(ConstPixelDims,
                            dtype='float16')



    ch_index = 0
    lstPathsDCM = dicom_dirs

    for z, pathDCMch in enumerate(lstPathsDCM):
        # read the files
        ds = pydicom.dcmread(pathDCMch)
        # store the raw image data
        try:
            ArrayDicom[:, :, z, ch_index] = ds.pixel_array
        except NotImplementedError:
            decomp = True
            print("meet a problem")

    mx = [np.max(ArrayDicom[..., ch]) for ch in range(channels)]
    ArrayDicom = np.divide(ArrayDicom, mx)


    save_path = '/no_backup/d1259/excel/NAKO_IQA/Arrays/' +  filename[35:38]+ '_' + os.path.basename(COMPOSED_folder)
    np.save(save_path , ArrayDicom)


for COMPOSED_folder in COMPOSED_folders:
    get_array(COMPOSED_folder,filename)
