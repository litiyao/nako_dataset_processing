import os
import pydicom
import numpy as np

def get_Patient(directory):
    AFFsInNAKO = os.listdir(directory)  # get all files and folders names in the specified directory NAKO
    AFFsInNAKO.sort()
    Patient_slices = []
    for slice in AFFsInNAKO:
        if len(slice) == 92:
            Patient_slices.append(os.path.join(os.path.abspath(directory), slice))
    return Patient_slices

patient_slices = get_Patient('/home/d1259/med_data/NAKO/NAKO_IQA/Q3/dicom_sorted/3D_GRE_TRA_bh_in_0007')



filename = '/home/d1259/med_data/NAKO/NAKO_IQA/Q1/dicom_sorted/'
AllSequenceFolders = []
AFFsInPatientFolder = os.listdir(filename)  # list of AFFs (all files and folders) in one patient directory
for AFF in AFFsInPatientFolder:
    if os.path.isdir(os.path.join(filename, AFF)):
        AllSequenceFolders.append(os.path.join(filename, AFF))

for folder in AllSequenceFolders:
    AFFsInNAKO = os.listdir(folder)  # get all files and folders names in the specified directory NAKO

    AFFsInNAKO.sort()
    path_first_slice = os.path.join(os.path.abspath(folder), AFFsInNAKO[0])
    RefDs = pydicom.dcmread(path_first_slice)
    PatientName = RefDs.PatientName
    print(PatientName)
