import os
import shutil

def get_Patient(directory):
    AFFsInNAKO = os.listdir(directory)  # get all files and folders names in the specified directory NAKO
    AFFsInNAKO.sort()
    Patient_slices = []
    for slice in AFFsInNAKO:

        Patient_slices.append(os.path.join(os.path.abspath(directory), slice))
    return Patient_slices

lst_npy = get_Patient('/home/d1259/no_backup/d1259/excel/NAKO_IQA/Arrays')

Fat_npy = []
for npy in lst_npy:
    if 'F' in npy:
        Fat_npy.append(npy)

'''name = os.path.basename(Fat_npy[0])
copy_path = '/home/d1259/no_backup/d1259/Fat/' + os.path.basename(Fat_npy[0])
shutil.move(Fat_npy[0], copy_path)'''

for npy in Fat_npy:
    name = os.path.basename(npy)
    copy_path = '/home/d1259/no_backup/d1259/Fat/' + os.path.basename(npy)
    shutil.move(npy, copy_path)