import os
import pydicom
import numpy as np

def get_Patient(directory):
    AFFsInNAKO = os.listdir(directory)  # get all files and folders names in the specified directory NAKO
    AFFsInNAKO.sort()
    Patient_slices = []
    for slice in AFFsInNAKO:
        if len(slice) == 92:
            Patient_slices.append(os.path.join(os.path.abspath(directory), slice))
    return Patient_slices

patient_slices = get_Patient('/home/d1259/med_data/NAKO/NAKO_IQA/Q3/dicom_sorted/3D_GRE_TRA_bh_opp_0006')

dicom_dirs = patient_slices

RefDs = pydicom.dcmread(patient_slices[0])
p = int(RefDs.Rows)
channels = 1
ConstPixelDims = (int(RefDs.Rows),  # 行数 对我来说 260
                      int(RefDs.Columns),  # 列数 对我来说 320
                      len(dicom_dirs),  # slice 数量 高度 316
                         channels)

ArrayDicom = np.zeros(ConstPixelDims,
                        dtype='float16')



ch_index = 0
lstPathsDCM = dicom_dirs

for z, pathDCMch in enumerate(lstPathsDCM):
    # read the files
    ds = pydicom.dcmread(pathDCMch)
    # store the raw image data
    try:
        ArrayDicom[:, :, z, ch_index] = ds.pixel_array
    except NotImplementedError:
        decomp = True
        print("meet a problem")

np.save('NAKO_IQA/ArrayDicom', ArrayDicom)


import os
import time
import subprocess
import numpy as np
import pickle
import pandas
import pydicom
import numpy
import matplotlib

import matplotlib.pyplot as plt
from matplotlib import gridspec




def plot_patient_slices(ch, dim,npy_path):
    """Function for plotting patient with label."""
    # get patient data
    dicoms = np.load(npy_path)  # dicoms是四维array niis 是标签
    x, y, z = dicoms.shape[:3]  # x= 260, y = 320, z = 316
    # arrays for plotting
    dicoms = (dicoms).astype('float32')

    # channel: fat = 0, water = 1
    chNr = 0
    if ch == 'water':
        chNr = 1

    # plot cuts in each dim
    fig = plt.figure(figsize=(18, 7))
    gs = gridspec.GridSpec(1, 3,
                           width_ratios=[x / y, z / y, x / z],
                           height_ratios=[1])
    ax1 = plt.subplot(gs[0])
    plt.axis('off')
    # plt.xlabel('x-axis')
    # plt.ylabel('y-axis')
    ax1.imshow(np.fliplr(np.rot90(dicoms[:, :, dim[2], chNr], axes=(1, 0))), interpolation='none', cmap='gray')
    ax2 = plt.subplot(gs[1])

    plt.axis('off')
    # plt.xlabel('z-axis')
    # plt.ylabel('y-axis')
    ax2.imshow(dicoms[dim[0], :, :, chNr], interpolation='none', cmap='gray')
    ax3 = plt.subplot(gs[2])
    plt.axis('off')
    # plt.xlabel('x-axis')
    # plt.ylabel('z-axis')
    ax3.imshow(np.fliplr(np.rot90(dicoms[:, dim[1], :, chNr], axes=(1, 0))), interpolation='none', cmap='gray')
    plt.show()


def get_Patient(directory):
    AFFsInNAKO = os.listdir(directory)  # get all files and folders names in the specified directory NAKO
    AFFsInNAKO.sort()
    Patient_slices = []
    for slice in AFFsInNAKO:

        Patient_slices.append(os.path.join(os.path.abspath(directory), slice))
    return Patient_slices

lst_npy = get_Patient('/home/d1259/no_backup/d1259/Fat/')
lst_npy.sort()
ch = 'fat'
dim = (130, 160, 118, 1)
for npy in lst_npy:
    plot_patient_slices(ch, dim, npy)
pickle.dump(lst_npy, open( "patients_Fat.p", "wb" ) )