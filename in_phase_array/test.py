import os
import numpy as np

AllArrays = []
AFFsInSequenceFolder = os.listdir('/home/d1259/no_backup/d1259/excel/InPhase3DImages_no_norm/')
for Image in AFFsInSequenceFolder:
    if os.path.isfile(os.path.join('/home/d1259/no_backup/d1259/excel/InPhase3DImages_no_norm/', Image)):
        AllArrays.append(os.path.join('/home/d1259/no_backup/d1259/excel/InPhase3DImages_no_norm/', Image))

for Array in AllArrays:
    Array4D = np.load(Array)
    Array_In_Phase = Array4D[:, :, :, 0]
    name = os.path.basename(Array)
    path = '/home/d1259/no_backup/d1259/excel/array_in_phase_no_norm/' + name
    np.save(path, Array_In_Phase)
