#additional modules openxyl, xlrd
import os
import pydicom
import pandas as pd
import numpy as np

def GetPathOfAllImages(directory): # directory where the images are saved, for me directory = '/home/d1259/NAKO/'
    # get the array of the paths of all patient folders
    AFFsInNAKO = os.listdir(directory)  # get all files and folders names in the specified directory NAKO
    PatientFolders = []
    for PatientFolder in AFFsInNAKO:
        if os.path.isdir(os.path.join(os.path.abspath(directory), PatientFolder)): # check whether the current object is a folder or not
            PatientFolders.append(os.path.join(os.path.abspath(directory), PatientFolder)) #include 200 patient folders and 1 trash folder ('/home/d1259/NAKO/.Trash-1000')

    # get list of the paths of all sequence folders
    AllSequenceFolders = []
    for filename in PatientFolders:

        if os.listdir(filename):  #get rid of empty folder which stored under trash folder
            AFFsInPatientFolder = os.listdir(filename)  # list of AFFs (all files and folders) in one patient directory
            for AFF in AFFsInPatientFolder:
                if os.path.isdir(os.path.join(filename, AFF)):
                    AllSequenceFolders.append(os.path.join(filename, AFF))

    # get list of the paths of all images ALlImageData
    AllImageData = []
    for SequenceFolder in AllSequenceFolders:
        AFFsInSequenceFolder = os.listdir(
            SequenceFolder)  # list of AFFs (all files and folders) in one sequence directory
        for Image in AFFsInSequenceFolder:
            if os.path.isfile(os.path.join(SequenceFolder, Image)):
                AllImageData.append(os.path.join(SequenceFolder, Image))

    return AllImageData

# Attributes is list of wanted parameters, for me Attributes = ['PatientID', 'StudyDate', 'StudyTime', 'StudyInstanceUID', 'InstitutionName', 'ProtocolName']
# path to where the new excel should be saved (for me path = "/Users/tiyaoli/PycharmProjects/excel/NewExcel.xlsx")
# return dataframe parsing from all images and save it to new excel

def ParseAllImages(Attributes, AllImageData, path):

    LenAllImageData = len(AllImageData) #length of AllImageData
    DictofLists = {}
    for Attribute in Attributes:
        KeyName = Attribute + 'List'
        DictofLists[KeyName] = []
        for DicomImage in AllImageData:
            ds = pydicom.dcmread(DicomImage) # read in dicom image
            a = ds.get(Attribute)
            DictofLists[KeyName].append(a)

    FinalArray = np.zeros((1, LenAllImageData)) #create a single row array with 0s
    for k in DictofLists:                       # stack the lists together, forming a 2D array
        CurrentArray = np.asarray(DictofLists[k])
        FinalArray = np.vstack((FinalArray, CurrentArray))

    FinalArray = np.transpose(FinalArray)  # transpose
    FinalArray = FinalArray[:, 1:len(Attributes) + 1] # get rid of the first column full of 0s
    df = pd.DataFrame(FinalArray, columns=[Attribute for Attribute in Attributes]) # convert array into a dataframe
    df.to_excel(path, index=False) # write the dataframe to a new excel
    return df

# PathOfOldExcel (for me '/Users/tiyaoli/PycharmProjects/excel/visualQADump-MEVISDB_2014-2015-2016.v2.xlsx')
# SheetName (for me 'visQA_GRE3D' )
# combine PatientID_ExcelFile and QualityRating from old excel in new excel, get a CombiExcel
# return the final dataframe
# PathOfCombiExcel (for me "/Users/tiyaoli/PycharmProjects/excel/CombiExel.xlsx")
def CombiExcel(PathOfOldExcel, SheetName, PathOfNewExcel, PathOfCombiExcel):
    # read in old excel
    OldExcel = pd.read_excel(PathOfOldExcel, sheet_name=SheetName)
    PartOfOldExcel = OldExcel['studyInstanceUID']  # choose studyInstanceUID from old excel
    PartOfOldExcel.columns = ['StudyInstanceUID']  # rename the label to keep consistent with label in new excel obtained by parsing images

    # read in new excel
    NewExcel = pd.read_excel(PathOfNewExcel)
    PartOfNewExcel = NewExcel['StudyInstanceUID']

    # get the list of PatientID_ExcelFile from old excel matched into new excel
    PatientID_ExcelFile = []
    NumberOfRowsNE = len(NewExcel.index)  # count rows in new excel
    NumberOfRowsOE = len(OldExcel.index)  # count rows in old excel
    for RowInNE in range(NumberOfRowsNE):  # RowInNE row in new excel
        for RowInOE in range(NumberOfRowsOE):
            row1 = PartOfNewExcel.iloc[RowInNE]
            row2 = PartOfOldExcel.iloc[RowInOE]
            if row1 == row2:
                PatientIDInNE = OldExcel.at[RowInOE, 'patid']
                PatientID_ExcelFile.append(PatientIDInNE)

    # get the list of QualityRating from old excel matched into new excel
    QualityRating = []
    NumberOfRowsNE = len(NewExcel.index)  # count rows in new excel
    NumberOfRowsOE = len(OldExcel.index)  # count rows in old excel
    for RowInNE in range(NumberOfRowsNE):  # RowInNE row in new excel
        for RowInOE in range(NumberOfRowsOE):
            row1 = PartOfNewExcel.iloc[RowInNE]
            row2 = PartOfOldExcel.iloc[RowInOE]
            if row1 == row2:
                QRInNE = OldExcel.at[RowInOE, 'qualityRating']
                QualityRating.append(QRInNE)

    NewExcel.insert(0, 'PatientID_ExcelFile', PatientID_ExcelFile)  # add PatientID_ExcelFile as the first column
    NewExcel['QualityRating'] = QualityRating  # add QualityRating as the last column
    NewExcel = NewExcel.rename(columns={'PatientID': 'PatientID_FolderName', 'ProtocolName': 'Protocol'})  # rename

    # write the dataframe to excel with the name CombiExcel
    NewExcel.to_excel(PathOfCombiExcel, index=False)
    return NewExcel

# main function
AllImageData = GetPathOfAllImages("/home/d1259/NAKO/") # directory where the images are saved
AllImageData = AllImageData[0:100]

Attributes = ['PatientID', 'StudyDate', 'StudyTime', 'StudyInstanceUID', 'InstitutionName', 'ProtocolName'] # attributes that wir are intrested in in images
path = "/home/d1259/no_backup/d1259/excel/NewExcel_100.xlsx" # path to where the new excel should be saved
ParseAllImages(Attributes, AllImageData, path)
PathOfOldExcel = "/home/d1259/no_backup/d1259/excel/visualQADump-MEVISDB_2014-2015-2016.v2.xlsx"
SheetName = 'visQA_GRE3D'
PathOfNewExcel = "/home/d1259/no_backup/d1259/excel/NewExcel_100.xlsx"  # save path of the new excel
PathOfCombiExcel = "/home/d1259/no_backup/d1259/excel/CombiExcel_100.xlsx"  # save path of the combined excel
CombiExcel(PathOfOldExcel, SheetName, PathOfNewExcel, PathOfCombiExcel)







