
import pandas as pd


def CombiExcel(PathOfOldExcel, SheetName, PathOfNewExcel, PathOfCombiExcel):
    # read in old excel
    OldExcel = pd.read_excel(PathOfOldExcel, sheet_name=SheetName)
    PartOfOldExcel = OldExcel['studyInstanceUID']  # choose studyInstanceUID from old excel
    PartOfOldExcel.columns = ['StudyInstanceUID']  # rename the label to keep consistent with label in new excel obtained by parsing images

    # read in new excel
    NewExcel = pd.read_excel(PathOfNewExcel)
    PartOfNewExcel = NewExcel['StudyInstanceUID']

    # get the list of PatientID_ExcelFile from old excel matched into new excel
    PatientID_ExcelFile = []
    NumberOfRowsNE = len(NewExcel.index)  # count rows in new excel
    NumberOfRowsOE = len(OldExcel.index)  # count rows in old excel
    for RowInNE in range(NumberOfRowsNE):  # RowInNE row in new excel
        for RowInOE in range(NumberOfRowsOE):
            row1 = PartOfNewExcel.iloc[RowInNE]
            row2 = PartOfOldExcel.iloc[RowInOE]
            if row1 == row2:
                PatientIDInNE = OldExcel.at[RowInOE, 'patid']
                PatientID_ExcelFile.append(PatientIDInNE)

    # get the list of QualityRating from old excel matched into new excel
    QualityRating = []
    NumberOfRowsNE = len(NewExcel.index)  # count rows in new excel
    NumberOfRowsOE = len(OldExcel.index)  # count rows in old excel
    for RowInNE in range(NumberOfRowsNE):  # RowInNE row in new excel
        for RowInOE in range(NumberOfRowsOE):
            row1 = PartOfNewExcel.iloc[RowInNE]
            row2 = PartOfOldExcel.iloc[RowInOE]
            if row1 == row2:
                QRInNE = OldExcel.at[RowInOE, 'qualityRating']
                QualityRating.append(QRInNE)

    NewExcel.insert(0, 'PatientID_ExcelFile', PatientID_ExcelFile)  # add PatientID_ExcelFile as the first column
    NewExcel['QualityRating'] = QualityRating  # add QualityRating as the last column
    NewExcel = NewExcel.rename(columns={'PatientID': 'PatientID_FolderName', 'ProtocolName': 'Protocol', 'StationName': 'InstitutionName'})  # rename

    # write the dataframe to excel with the name CombiExcel
    NewExcel.to_excel(PathOfCombiExcel, index=False)
    return NewExcel

# main function

PathOfOldExcel = '/home/d1259/no_backup/d1259/ExcelTest/visualQADump-MEVISDB_2014-2015-2016.v2.xlsx'
SheetName = 'visQA_GRE3D'
PathOfNewExcel = '/home/d1259/no_backup/d1259/excel/NewExcel_for_AI/NewExcel.xlsx'
PathOfCombiExcel = '/home/d1259/no_backup/d1259/excel/NewExcel_for_AI/CombiExcel1.xlsx'
CombiExcel(PathOfOldExcel, SheetName, PathOfNewExcel, PathOfCombiExcel)